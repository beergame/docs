# Beergame Dokumentation

## Allgemeines
* Diese Dokumentation basiert auf Textfiles, die die [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax) benutzen
* Um aus dem Markdownfile eine HTML-Dokumentation zu erzeugen, wird [Pandoc](http://johnmacfarlane.net/pandoc/) verwendet. 
* Das Aussehen der Dokumentation wird über das CSS-File beeinflusst, das in diesem Repository zu finden ist.

## Generieren der Ausgabedateien
Der folgende Befehl generiert aus der Markdowndatei beergame.md die Datei beergame.html im Unterverzeichnis "out" (ACHTUNG bei Nicht-Unix-Systemen muss dieser Ordner manuell angelegt werden). Hierbei müssen die Berechtigungen des Verzeichnises berücksichtig werden.

    pandoc -s -S --toc -c markdown.css beergame.md -f markdown -t html --self-contained -o out/beergame.html


## Richtlinien für die Erstellung dieser Dokumentation
* Die Dokumentation wird in Deutsch verfasst.