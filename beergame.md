---
title: 'Bergame Documentation'
subtitle: 'The Beer Distribution Game in node.js'
date: 'Version: 0.1'
authors: 'Benedict Lang, Christian Schürings, Daniel Föhr, Max Kuchenbecker'
---

# Allgemeines
Dieses Projekt ist das Ergebnis eines Semesterprojekts des Studiengangs Wirtschaftsinformatik an der DHBW Ravensburg im Jahrgang RV-WWI-113 (2016).

## Zweck dieses Dokuments
Dieses Dokument ist die technische Dokumentation für das Beergame-Projekt. Diese Dokumentation richtet sich an eine technische Zielgruppe und soll die Möglichkeit bieten, das Projekt unabhängig vom bisherigen Team weiterzuentwickeln und zu verbessern. Dabei beschreibt es sowohl die eingesetzten Technologien im Allgemeinen als auch konkrete Zusammenhänge dieses Projektes.

## Getting Started
Der Sourcecode des Projektes findet sich bei [Bitbucket](https://bitbucket.org/beergame/beergame/). In diesem Repository ist in der Readme-Datei auch ein Abschnitt enthalten, der beschreibt, wie erste Schritte mit dem Projekt gemacht werden können und wie die Applikation verwendet wird.

## Das Bierspiel
Das vorliegende Projekt ist eine Implementierung des Beergame (auch Beer Distribution Game genannt), das in akademischen Kontexten zur Vermittlung des sogenannten Bullwhip-Effekts im Zusammenhang mit Supply-Chain-Management an Studierende verwendet wird. 

### Aufbau und Regeln
Ein vollständiges Regelwerk kann auf der Webseite [beergame.org](http://beergame.org/the-game) nachgelesen werden. Zusammengefasst nehmen mehrere Spieler (ein Spieler kann auch ein Team von Spielern sein) an einer Lieferkette teil. Dabei wird der Wertschöpfungsprozess von Bierkästen vom Brauen in der Brauerei bis zum Verkauf im Getränkemarkt abgebildet. Jeder Spieler versucht eingehende Bestellungen zu bedienen, indem er selbst Bestellungen bei der vorhergehenden Einheit innerhalb der Lieferkette aufgibt. Dabei muss ein Spieler darauf achten, die Summe aus Strafzahlungen (1 Geldeinheit pro Wareneinheit) bei nichterfüllter Bestellung und Lagerkosten (0,5 Geldeinheiten pro Wareneinheit) für nicht abgerufene Ware möglichst gering zu halten.

## Anforderungen an das Projekt
Die folgenden Anforderungen wurden von Seiten des Auftraggebers an das Projekt gestellt:

* Das Ziel des Projektes ist es, eine Anwendung zu entwickeln, die es ermöglicht, das Beergame mit mehreren Studierenden in der Vorlesung zu spielen. Dabei sollen keine Hilfsmittel wie Excel-Listen - die bisher eingesetzt wurden - mehr nötig sein.
* Das Spiel soll auf allen mobilen Endgeräten (Laptops, Smartphones, Tablets) mit geringem Aufwand gespielt werden können.
* Der Quellcode der Anwendung ist gut zu dokumentieren und so zu strukturieren, dass eine Weiterentwicklung auch unabhängig vom ursprünglichen Team möglich ist. Hierzu dient auch das vorliegende Dokument.
* Die Wahl der Technologien und Umsetzung ist dem Team überlassen.

# Verwendete Technologien
In diesem Abschnitt werden die im Projekt verwendeten Technologien beschrieben. Darüber hinaus wird begründet, warum die Entscheidung auf die jeweilige Technologie gefallen ist.

## Javascript & node.js
Zu Beginn des Projektverlaufes wurde Javascript als Programmiersprache für das Projekt festgelegt. Dies hat mehrere Gründe:

* Innerhalb des Projektteams war entweder bereits Know-How oder die Bereitschaft zum Erlernen von Javascript vorhanden, da die Sprache weit verbreitet ist.
* Die Anforderungen des Projekts erforderten eine Technologie, die einen dynamischen Austausch zwischen Server und Client (Browser) ermöglicht, um die Spielstände zwischen den einzelnen Clients zu verteilen und damit einen Spielablauf zu ermöglichen. Node.js ermöglicht es, diese Anforderung sowohl im Frontend als auch im Backend mit der selben Technologie (Javascript bzw. socket.io) zu erfüllen.
* Nodes.js ist leicht zu installieren und benötigt wenige Ressourcen und Abhängigkeiten. Der Entwicklungsprozess ist schnell und dynamisch, es muss kein Compiler verwendet werden, Änderungen am Code sind sofort in der Applikation sichtbar.

## MongoDB
MongoDB ist eine weit verbreitete NoSQL-Datenbank, die JSON-Dokumente speichern kann. Beim Einsatz von Express.js (Erklärung s. unten) ist es üblich, eine MongoDB zu verwenden. Der Einsatz wird damit begründet, dass serialisierte Javascript-Objekte (JSON) einfach in der Datenbank abgelegt werden können und auch die Suche und die Änderung von gespeicherten Dokumenten durch diese Kompatibilität für den Entwickler im Programmcode enorm vereinfacht wird. Darüber hinaus ist MongoDB als NoSQL-Datenbank flexibel im Bezug auf das Datenschema, was sich im Lauf des Entwicklungsprozesses als sehr hilfreich herausgestellt hat. Mehr Informationen findet sich auf der Webseite des [Projektes](https://www.mongodb.com/).

### Mongoose
Mongoose ist eine Library für Node.js, die es ermöglicht, Datenbankzugriffe mit einer Modellierungssprache zu kapseln. Somit ersetzen Methodenaufrufe von Mongoose das Formulieren und Auswerten von Anfragen an Mongoose. Eine vollständige Dokumentation findet sich auf der Webseite des [Projektes](http://mongoosejs.com/docs/guide.html).

Mongoose erfordert die Definition von Schemata, die beschreiben, wie eine Entität eines Objektes aussieht:

```
// /database/schema/supplyChainSchema.js

var mongoose = require('mongoose');

var supplyChainSchema = mongoose.Schema({
    id: Number,
    game: { type: mongoose.Schema.Types.ObjectId, ref: 'Game' }
});

module.exports = supplyChainSchema;

```

Dieses Schema wird daraufhin von einem Model wiederverwendet:

```
// /database/model/SupplyChain

var mongoose = require('mongoose');
var supplyChainSchema = require('../schema/supplyChainSchema.js');

var SupplyChain = mongoose.model('SupplyChain', supplyChainSchema);

module.exports = SupplyChain;

```

Um nun in der Applikation ein SupplyChain-Objekt in der Datenbank zu erstellen kann folgender Code verwendet werden:

```
supplyChain = new SupplyChain({'id': i, 'game': game});
supplyChain.save(function saveSupplyChain(err) {
    if(err) {
		// handle error
    }
    
    // supplyChainObject was saved successfully.
});
```

## Express.js
Express.js ist ein Framework, das die Verwendung von Node.js für Webanwendungen erleichtert. Es orientiert sich am MVC-Pattern und gibt dem Entwickler diverse Hilfen an die Hand. Eine vollständige Dokumentation findet sich auf der Webseite des [Projektes](http://expressjs.com/). Hier sind die wichtigsten Merkmale für ein grundlegendes Verständnis kurz herausgegriffen.

Express.js wurde ausgewählt, 

* da es für die Erstellung von Webanwendungen mit Node.js sehr weit verbreitet ist,
* einen sehr einfachen Einstieg bietet,
* mit der Projektstuktur sehr flexibel ist und sich an die Vorstellungen der Entwickler anpasst sowie
* sehr viele Erweiterungen und guten Support im Internet bietet.


### Request and Response
Express arbeitet mit Requests und Responses. Ein Request ist dabei ein Aufruf eines Useragents, der unter einer bestimmten URL auf einem bestimmten Port terminiert. Auf diesem Port wartet die Express-App auf Anfragen und bearbeitet diese intern. Dem Benutzer wird immer ein Response-Objekt zurückgegeben, das neben diversen Headern (HTTP-Response-Code) auch Content in Form von JSON oder einem HTML Dokument enthält.

### Middlewares
Ein Request durchläuft, wenn er die App durchläuft, mehrere Middlewares. Die Reihenfolge der Middlewares ist dabei durch die Anwendung vorgegeben. Eine Middleware hat immer Zugriff auf das Request- und das Responseobjekt und kann diese beliebig manipulieren, Informationen anhängen oder auslesen. Ein Beispiel für eine Middelware ist das Routing. Der Router überprüft dabei, welche Endung die aufgerufene URL hat und führt demnach eine entsprechende Aktion aus.

Die verschiedenen Middlewares werden im File app.js registriert. Im folgenden Codebeispiel wird ein Node-Modul als Middleware registriert, das Express bereits standardmäßig mitliefert und das den HTTP-Body parst, wenn dieser ein JSON Objekt enthält.

```
app.use(bodyParser.json());
```

#### Codebeispiel einer Routingmiddleware

Das nachfolgende Beispiel zeigt das Routing für die Landingpage der Beergame Anwendung. In der app.js wird dabei zuerst die Midleware registriert. Dabei wird mit dem ersten Parameter mitgeteilt, für welche URL-Endpunkte die Middleware aufgerufen werden soll. Der zweite Parameter beschreibt das Nodemodul, das ausgeführt werden soll - also die Middleware.

```
var routes = require('./routes/index');
app.use('/', routes);
```
 
Die konkrete Middleware sieht folgendermaßen aus:
 
```
/* set up express */
var express = require('express');
var router = express.Router();

/* landing page */
router.get('/', function handleGetLandingPageRoute(req, res) {
  res.render('index');
});

module.exports = router;
```
In diesem einfachen Fall wird lediglich eine View (index) gerendert, es ist jedoch denkbar, dass an dieser Stelle komplexe Applikationslogik ausgeführt wird. Die Logik kann auch in eigene Controller ausgelagert werden, um die Trennung nach dem MVC-Pattern einzuhalten. Innerhalb der Middleware wird dann lediglich die Controlleraktion ausgerufen.

Abschließend sei gesagt, dass eine Middleware jeden beliebigen Zweck erfüllen kann. Authentifizierung von Requests ist genauso eine Middelwareaufgabe wie das Anhängen von applikationsspezifischen HTTP-Headern.

## socket.io
Socket.io ist eine eventbasierte Erweiterung für Javascript, die bidirektionale Kommunikation zwischen Clients und Servern in Echtzeit ermöglicht. Auf dem Server lassen sich Events auslösen, die von einem Eventlistener im Client behandelt werden und umgekehrt. Verwendet wird socket.io für die Kommunikation zwischen den einzelnen Spielern und dem Server während des Spiels, um z.B. eine Bestellung aufzugeben. Eine vollständige Dokumentation findet sich auf der Webseite des [Projektes](http://socket.io/docs/).

## materialize.css
Für die Darstellung im Frontend wird materialize.css eingesetzt. Dabei handelt es sich um ein responsives CSS-Framework, das die Anforderungen von Googles Material Design implementiert. Es bietet neben einem Grid-System auch die einfache Verwendung von Farben und definiert Aussehen für verschiedene Komponenten wie Tabellen, Buttons und Infoboxen. Eine vollständige Dokumentation findet sich auf der Webseite des [Projektes](http://materializecss.com/).

## ESLint
Bei der Entwicklung wurde ESLint eingesetzt, um Codekonventionen zu validieren. In der Datei .eslintrc finden sich verschiedene Vorgaben und Konventionen zum Thema Programmierstil. Vor jedem Commit wurde überprüft, ob diese vereinbareten Standards eingehalten worden sind.

Zusäztlich dazu wurde auch Editorconfig eingesetzt. Hier finden sich Einstellungen, die für die jeweilige Programmierumgebung festlegt, wie beispielsweise Tabulatoren verwendet werden. Dies hilft, den definierten Programmierstil automatisiert anzuwenden und vermeidet zum Beispiel Unübersichtlichkeiten bei dem Vergleich von Code bei einem Pull-Request.


# Datenmodell und Funktionsweise des Spiels

Dieser Abschnitt erklärt kurz das Datenmodell und soll damit einen Einstieg in den Programmcode erleichtern.

## Game
Die größte strukturierende Einheit ist ein Game. Ein Spiel umfasst dabei mehrere Supply Chains und Spieler. Ein Spiel ist gleichzusetzen mit einem Vorlesungstermin, bei dem die Simulation mit einer Gruppe von Studierenden umgesetzt werden soll. Ein Spiel kann von einem User angelegt und verwaltet werden.

## SupplyChain
Innerhalb eines Spiels kann es mehrere Supply Chains geben. Der User kann die Anzahl an Supply Chains in den Spieleinstellungen konfigurieren. Jede Supply Chain umfasst dabei die gleiche Anzahl an Rollen. Ein Spieler belegt später eine einzelne Rolle in genau einer Supply Chain. Die Anzahl der Supply Chains ist damit eine Möglichkeit, die Simulation an die Gruppengröße anzupassen.

## User
Ein User kann sich beim Beergame mithilfe einer E-Mail-Adresse und eines Passworts registrieren. Er muss nach der Registrierung seine Anmeldung per E-Mail bestätigen und kann sich dann in den Userbereich einloggen. Dort kann er Spiele erstellen, Spiele bearbeiten und Spiele löschen sowie sich nachträglich erneut die Auswertung von Spielen anschauen.

Dem User kommt im Spiel eine gesonderte Rolle zu. Während die Spieler sich alle mit einem Token und einem Benutzernamen für ein Spiel anmelden und dabei eine Rolle in einer bestimmten Supply Chain belegen, spielt der User auf mehreren Supply Chains innerhalb eines Spiels auf der Position 0 und seine Bestellungen im Rahmen des Spiels sind für alle Supply Chains innerhalb eines Spiel gültig. Der User verfügt über einen anderen Spielbildschirm als die Spieler und kann z.B. ein Ende des Spiels auch vor Ablauf der eingestellten Runden herbeiführen. 

Wichtig ist, dass der User im Rahmen des Spiels zusätzlich zu seiner Rolle als User auch eine Instanz des Objektes Player ist, um die Komplexität der Applikationslogik innerhalb der Kalulation nicht noch größer zu machen. Das Erstellen der Rolle passiert für den Benutzer transparent.

## Player
Ein Player ist ein Spieler, der im Rahmen der Simulation am Beergame teilnimmt. Er vergibt einen Benutzernamen und kann mithilfe eines Tokens an einem Spiel teilnehmen. Innerhalb des Spiels wählt er eine Rolle aus und wird damit Teil genau einer Supply Chain. Er kann in jeder Runde Bestellungen aufgeben und am Ende des Spiels die Auswertungen anschauen.

## Order
Eine Order ist eine Bestellung, die im Rahmen des Spiels aufgegeben wird. Jede Order ist zu einem Spieler und einer Runde zugehörig und kann damit innerhalb der Kalkulation für die Berechnung der relevanten Informationen verwendet werden.

## Delivery
Die Delivery ist im Datenmodell ein Hilfskonstrukt, das die Kalkulation erleichtert, indem Zwischenergebnisse (Outgoing Deliveries) in der Datenbank persistiert werden. Die Inputparameter für die Kalkulation (Game, Players, Orders und Deliveries) können somit aus den Informationen aus dem Spiel aus der Datenbank selektiert werden, was wesentlich einfacher und übersichtlicher für die Berechnung (siehe Funktion GameController.getCalculationData()) ist, als wenn die Deliveries als Eingabeparameter für alle Runden jedesmal erneut berechnet werden müssen (rekursiver Aufruf der Funktion).


# Aufbau des Projektordners
Express.js bietet dem Entwickler eine große Freiheit für die Gestaltung der Projektstruktur. Dieser Abschnitt erklärt kurz die Projektstruktur, die für das Beergame verwendet wird.

```
|-- bin
|	www
|-- config
|-- controllers
|-- database
|	-- model
|	-- schema
|-- helpers
|-- middlewares
|-- node_modules
|-- public
|	-- images
|	-- scripts
|	-- stylesheets
|-- routes
|-- views
|	-- auth
|	-- games
|	-- ...
|.eslintrc.js
|.gitignore
|.jsbeautifyrc
|app.js
|package.json
|README.md
```

Die Datei bin/www wird von Express mitgeliefert und ist dafür verantwortlich, dass Express auf einem definierten Port (3000) erreichbar ist und dort auf die Requests von Clients wartet und reagiert.

Im Ordner config sind verschiedene Dateien enthalten, die Konfigurationen für verschiedene Umgebungen darstellen. Die Umgebung muss dabei als NODE_ENV Variable vor dem Start der App definiert werden, die entsprechende Konfigurationsdatei wird dann automatisch geladen (siehe README.md im Repository). Die Datei default.json gibt zeigt, welchem Schema die Configfiles folgen müssen und welche Parameter konfiguriert werden können.

Der Ordner controllers enthält die Logik der Anwendung. Nahezu alle Routes, die eine gewisse Komplexität aufweisen rufen als Aktion eine Funktion eines Controllers auf. Dadurch ist die komplexe Applikationslogik aus den Routes-Files ausgelagert und kann unabhängig von den Routes editiert werden. Die Controller sind thematisch strukturiert.

Der Inhalt des Ordners database besteht in erster Linie aus Code, der aus Abhängigkeit zu Mongoose resultiert. Die Datei connection.js ist für das Etablieren einer Datenbankverbindung zur MongoDB verantwortlich, in den Ordnern schema und model finden sich die Implementierungen des Datenmodells für Mongoose (siehe Abschnitt Mongoose).

Unter helpers finden sich alle Funktionalitäten, die an mehreren Stellen in der Applikation verwendet werden. Beispielsweise sind hier der ErrorHandler und der Logger abgelegt.

Im Ordner middlewares befindet sich die Middlewarefunktionalität, die dafür verantwortlich ist, die Authentifizierung von Benutzern zu überprüfen.

Der Ordner node_modules wird von npm (Node Package Manager) erstellt und erhält alle Dependencies, die einzelne npm-Module benötigen (siehe package.json). An diesem Order werden keine manuellen Änderungen vorgenommen, er wird lediglich durch npm selbst verändert.

Im Ordner public befinden sich alle Dateien, die öffentlich (für die Clients) verfügbar sein sollen. Das beinhaltet in erster Linie Bilder und Stylesheets sowie Javascript-Files, die für Funktionalität im Client benötigt werden.

Im Ordner routes sind die bereits mehrfach angesprochenen Routesfiles enthalten. Diese sind thematisch geordnet und werden in der Datei app.js inkludiert.

Der Ordner views enthält die Darstellungsinformationen für Antworten, die der Browser vom Server bekommt (siehe MVC-Pattern). Auch die Views sind thematisch in Unterordner gegliedert.

Die Datei eslintrs.js enthält Regeln für den Validator eslint.

Die Datei ist ebenfalls von Express vorgeben und enthält in erster Linie die Integration der Middlewares (siehe Abschnitt Express).

Die Datei package.json enthält Informationen über die Dependencies, die die Applikation benötigt. Diese werden dann durch npm (Node Package Manager) installiert.

Die Datei README.md gibt Entwicklern die wichtigsten Hinweise zum Repository.



# Betrieb der Anwendung
## Serverinfrastruktur
Die Anwendung wurde zum Ende der Projektlaufzeit auf einen virtuellen Server aus der IT-Infrastruktur der DHBW Ravensburg deployed und wird dort betrieben. Der Zugriff auf den Server ist im LAN der DHBW Ravensburg möglich. Die IP-Adresse des Servers lautet 141.68.120.151.

Der Server ist nicht bootsave. Nach dem Neustart muss die MongoDB neugestartet werden und der PM2-Prozess erneut hinzugefügt werden.

## Konfiguration des Servers
Der Server wurde basierend auf einem Tutorial von [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-14-04) eingerichtet.

### Erweiterung der Rechte für Portvergabe
Standardmäßig ist es als Nicht-Root-User nicht möglich, Ports unterhalb von 1024 zu vergeben. Damit die Anwendung auf Port 80 (Standard für Webserver) ausgeliefert werden kann, wurde die folgende Konfiguration angewandt:

```
sudo apt-get install libcap2-bin
sudo setcap cap_net_bind_service=+ep /usr/local/bin/node
```

### Prozessmanager PM2
Als Prozessmanager für Node.js wird PM2 verwendet. PM2 ermöglicht es, Applikationen zu managen und als Deamon laufen zu lassen. Mithilfe von ```pm2 list``` lassen sich alle derzeit laufenden Prozesse anzeigen. Ist darin der Beergameprozess nicht enhalten, kann er mithilfe des folgenden Kommandos erzeugt werden (z.B. nach einem Reboot des Servers):


```
pm2 start /var/www/beergame/beergame/bin/www --name="beergame"
```

Der Name des Prozesses lautet "beergame". Mithilfe von PM2 lässt sich die Anwendung einfach stoppen und neustarten:

```
pm2 start beergame
pm2 stop beergame
```

### Applikationsverzeichnis
Die Applikation befindet sich im Ordner /var/www/beergame.

### Logging
Der Pfad des erzeugten Logfiles sowie das Log-Level lassen sich in der Konfiguration der Applikation einstellen. Der zur Übergabe eingestellte Pfad ist ```/var/log/beergame/beergame.log.JJJJ-MM-DD```.

#### Verwendung vom Elasticsearch-Logstash-Kibana Stack
Zur Auswertung der Logfiles wird der ELK Stack eingesetzt. Dieser ist auf dem Server installiert und die Oberfläche ist unter dem Port 5601 aufrufbar.

Zum Starten des Stacks sind folgende Befehle notwendig:
````sudo service elasticsearch start```
````sudo service logstash start```
````sudo service kibana start```

Der Stack wurde nach dieser Anleitung eingerichtet: [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elk-stack-on-ubuntu-14-04).

### MongoDB
Die Applikation verwendet eine MongoDB zur Persistierung der Daten. Der MongoDB-Service läuft auf dem Server unter dem Standardport (27017). Im Bezug auf die Sicherheit wurde der Einfachheit halber darauf verzichtet, einen expliziten User für die Datenbank zu konfigurieren, weil nur eine Applikation auf dem Server läuft. Die MongoDB wurde so konfiguriert, dass lediglich Verbindungen von localhost (127.0.0.1) zugelassen sind, das bedeutet sie ist von außerhalb des Servers nicht erreichbar. Hat ein User Zugriff auf den Server kann davon ausgegangen werden, dass er auch eine mit Usernamen und Passwort geschützte Datenbank auslesen kann (Files kopieren, o.ä.). Es erfolgt demnach keine Authentifizierung der Express-App gegenüber der MongoDB.


## Deployment
Wenn eine neue Version des Beergame auf den Server ausgespielt werden soll, müssen die folgenden Schritte befolgt werden:

- Im Ordner ```/var/www/beergame``` die neue Version der Applikation mithilfe von git herunterladen (```git pull```). 
- Die Dependencies mithilfe von npm installieren: ```npm i```.
- Abschließend muss der Prozess neu gestartet werden (```pm2 restart beergame```).

# Testing

Für das Beergame Projekt sind Integrationstest geschrieben worden, bei denen es darum geht, die Website-Funktionalitäten zusammenhängend zu testen. Es handelt sich hierbei nicht (!) um Unit Tests, d.h. es finden Operationen auf der Datenbank statt und es können unter Umständen Spuren der Tests in der Datenbank zurückbleiben. Dies ist darauf zurückzuführen, dass es vermieden wurde, die Datenbank nach den Tests vollständig zu löschen. Sollte in der Zukunft ein CI-Server für die weitere Entwicklung eingesetzt werden, macht es durchaus Sinn, eine seperate Datenbank für die Tests zu verwenden und die Integrationstests entsprechend anzupassen.
Zusätzlich zu den Integrationstests gibt es jedoch auch ein Unit Test Modul (validator.js), welches die Funktionalität des E-Mail Validators, sowie des Game-Validators testet.

## Ausführung der Tests

1. Sicherstellen, dass die MongoDB gestartet ist
2. Öffnen einer Konsole und starten der Beergame-Applikation:
        ```
        npm start
        ```
3. Öffnen einer zweiten Konsole und starten der Tests:
        ```
        npm test
        ```

## Aufbau des Tests-Ordners

Der test Ordner enthält 2 Dateien, die vom Endanwender aus betrachtet verschiedenste Funktionalitäten auf die jeweiligen Rollen verteilt (user.js / player.js).
Für Details, welche spezifischen Funktionalitäten in den Tests abgedeckt worden sind, wird an dieser Stelle auf den Quellcode bzw. die Ausführung der Tests verwiesen. Die Gliederung der Tests orientiert sich stark an den unten genannten Anforderungen.
Der Test (routing.js) führt einen Check durch, ob alle von der Anwendung verwendeten Routen bzw. URLs erreichbar sind und eine HTTP 200 Statusmeldung zurückgesendet wird. Zusätzlich wird auch getestet, ob sich die Anwendung bei der Aufruf einer fehlerhaften URL, wie unten in den Anforderungen definiert, verhält.


# Anforderungen

Die Grundlage für das Projekt waren folgende Problemstellung und Rahmenbedingungen:

* Problem : Der sogenannte Bullwhip-Effect ist ein Phänomen welches in unternehmensübergreifenden Suppy Chains auftritt. Das Phänomen wurde bereits umfassend erforscht, wobei auch heute noch an offenen Fragestellungen weitergearbeitet wird. In der Lehre werden die relevanten Zusammenhänge oft in Logistik und Suppy Chain Management-Veranstaltungen vermittelt. Hierfür existieren einige Softwarepakete (oft Simulationssoftware) zur Veranschaulichung des Effekts und zum Experimentieren mit verschiedenen Parametern. Die meisten Softwarepakete sind jedoch technologisch veraltet oder besitzen Einschränkungen.

* Ziel: Ziel des Projektes ist es, eine Simulationssoftware (App) zu entwickeln, die an der DHBW Ravensburg in der Lehre eingesetzt werden kann. Neben der Visualisierung des Bullwhip-Effects sollen auch Parameter veränderbar sein, um verschiedene Lösungsansätze zu testen. Die Software soll Multi-User-fähig sein und einen Wettbewerb zwischen verschiedenen Teilnehmern (Gruppen / Rollen) ermöglichen.

* Rahmenbedingungen: Die Software muss auf der Infrastruktur der DHBW Ravensburg lauffähig sein. Die Software muss netzwerk- und mehrbenutzerfähig sein. Ein zentraler Bestandteil des Arbeitsergebnisses ist eine vollständige und verständliche Dokumention für Anwender (Dozent, Student) und mögliche andere Entwickler, die ggf. zu einem späteren Zeitpunkt die Software anpassen oder weiterentwicklen. Alle Bestandteile (source code, etc.) müssen abgegeben werden. Die Software wird als open source unter eine Creative Commons Lizenz gestellt (CC-BY-SA).  Die Art der Software (PC, Webbasiert, Mobile App), die Architektur und verwendete Programmiersprache müssen mit dem Auftraggeber abgestimmt werden. Eigene Vorschläge sind willkommen und werden nach Möglichkeit unterstützt.

Zusätzlich zu den funktionalen Anforderungen des klassischen Beergames werden im Folgenden weitergehende Anforderungen als kurze und knappe User Story festgehalten.

## Benutzer: User (Dozent)

* Als User möchte ich mich mit meiner E-Mail Adresse registrieren können.
* Wenn ich mich registriert habe, dann möchte ich eine Validierungsemail erhalten, mit der ich meine Anmeldung bestätige.
* Solange die E-Mail Adresse nicht bestätigt worden ist, erhalte ich beim Loginversuch den Hinweis, dass meine E-Mail Adresse bestätigt werden muss.
* Wenn ich mich mit meiner bestätigten E-Mail Adresse und richtigen Passwort anmelde, möchte ich auf der Übersichtsseite landen.
* Auf der Übersichtsseite möchte ich neue Spiele anlegen, bestehende Spiele bearbeiten und löschen, einem bestehenden Spiel beitreten und mich ausloggen können.
* Als User möchte ich mich mit einem eigenen Game Token als User bei einem Spiel anmelden und innerhalb des Spieles als Konsument Bestellungen aufgeben können.
* Als User möchte ich verschiedene Spieleinstellungen anpassen können.

## Benutzer: Player (Student)

* Als Spieler möchte ich mich mit einem vorgegebenen Game Token und einem selbst definierten Usernamen für ein Spiel anmelden.
* Ich möchte, dass kein Username bei einem Spiel doppelt vorkommt.
* Als Spieler möchte ich eine Rolle innerhalb der Lieferkette frei auswählen können.
* Ein Spieler soll automatisch einer freien Lieferkette zugewiesen werden.

## Technische Anforderungen

* Alle Benutzerangaben sollen serverseitig validiert werden.
* Alle E-Mail Adressen sollen nach RFC 5322 Official Standard validiert werden.
* Ein unregistrierter User muss beim Login abgelehnt werden.
* Wenn eine unbekannte E-Mail Adresse beim User Login eingegeben wird, dann muss eine 'Username or password invalid' Meldung angezeigt werden.
* Wenn ein falsches Passwort auf der User Login Seite eingegeben wird, dann muss eine 'Username or password invalid' Meldung angezeigt werden.

# Projektmanagement und Entwicklungsprozess
Im Rahmen der Umsetzung des Projektes wurde versucht, sich möglichst nah an den Ideen von Scrum zu orientieren. Das Projekt wurde schrittweise entwickelt, der Fokus lag dabei stets auf den aktuell anstehenden Funktionalitäten. Bei regelmäßigen Projektmeetings wurden offene Fragen diskutiert und die Priorität für die nächsten Schritte festgelegt. Insbesondere die Komplexität der Applikationslogik machte es hier jedoch schwer, kleine Aufgabenpakete zu definieren und Releases zu machen.

Die Übersicht über die noch offenen To Dos liefert dabei ein [Trello-Board](https://trello.com/b/cXhrBQyW/beergame-backlog). Die Trellokarten durchwanderten dabei mehrere Spalten innerhalb des Boards, je weiter der Arbeitsfortschritt war. In einer To-Do-Spalte wurden die offenen Anforderungen gesammelt. Die Aufgaben wurden bei den Projekttreffen stets verteilt und die Karten bei Beginn der Tätigkeit in die Liste "Doing" einsortiert. 

Nach Umsetzung der Anforderung wurde sie in die Spalte "Review" verschoben. Im Rahmen des Projektes wurde mit dem Entwicklungsprozess git flow gearbeitet, das bedeutet, dass für jede Karte ein Codereview in Form eines Pull-Requests stattfand. Je nach Komplexität der umgesetzten Codeänderung wurde die Änderung erst nach einem oder zwei Approves des Pull-Requests in die gemeinsame Codebasis (develop-Branch) zurückgeführt.

Sobald die Änderungen wieder in den develop-Branch zurückgeführt wurden, wurden die Trellokarten in die Liste "Erledigt" einsortiert.

Mithilfe von Zuweisungen an einzelne Teammitglieder konnte der Überblick über die Zuständigkeiten erhalten werden. Mit Labels und der Visualisierung von Ablaufdaten konnten noch mehr Informationen im Board schnell erfasst und abgebildet werden.